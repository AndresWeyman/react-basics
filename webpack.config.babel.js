const { resolve } = require('path')
const webpack = require('webpack')

module.exports = env => {
    const config = {
        watch: env.dev ? true : false,
        context: resolve('src'),
        entry: ['babel-polyfill', './client.js'],
        output: {
            filename: 'bundle.js',
            path: resolve('src/static/dist'),
            publicPath: '/dist/'
        },
        module: {
            rules: [
                { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" },
            ]
        }
    }
    return config
}