const express = require('express')

//SERVER SETUP
const app = express()
app.set('port', (process.env.PORT || 5000))
app.use(express.static(__dirname + '/static'))
app.set('views', __dirname + '/views')
app.set('view engine', 'ejs')

//SERVER'S ROUTES
app.get('*', function(req, res) {
	res.render('index.ejs')
})

//SERVER INIT
app.listen(app.get('port'), function() {
  	console.log('Node app is running on port', app.get('port'))
})