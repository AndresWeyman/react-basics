import React from 'react'
import { render } from 'react-dom'

render(
    <h1>Hi React!!</h1>,
    document.getElementById('root')
)